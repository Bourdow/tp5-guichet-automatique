package controller;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import model.Account;
import model.User;

public class ShowBalanceController {

	private User user;
	List<Account> accounts;
	private ResourceBundle messages;
	
	public ShowBalanceController(User user) {
		this.setUser(user);
		this.setAccounts(user.getAccounts());
		Locale currentLocale = new Locale(user.getLanguage(), user.getCountry());
		this.messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
	public ResourceBundle getMessages(){
		  return this.messages;
		}
}
