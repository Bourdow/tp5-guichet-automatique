package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.User;

public class ModifyProfileAction implements ActionListener{

	JFrame mainFrame;
	User user;
	JTextField password;
	JTextField FirstName;
	JTextField LastName;

	public ModifyProfileAction(JFrame mainFrame, User user, JTextField password, JTextField FirstName, JTextField LastName) {
		this.mainFrame = mainFrame;
		this.user = user;
		this.password = password;
		this.FirstName = FirstName;
		this.LastName = LastName;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		user.setPassword(password.getText());
		user.setFirstName(FirstName.getText());
		user.setLastName(LastName.getText());
		user.update();
		JOptionPane.showMessageDialog(mainFrame, "Changes saved !", "Info", JOptionPane.PLAIN_MESSAGE);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}
