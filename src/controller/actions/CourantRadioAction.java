package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

public class CourantRadioAction implements ActionListener{

	JButton btnValidate;
	JTextField txtFieldId;
	JButton btnVerify;
	
	public CourantRadioAction(JTextField txtFieldId, JButton btnVerify, JButton btnValidate) {
		this.txtFieldId = txtFieldId;
		this.btnVerify = btnVerify;
		this.btnValidate = btnValidate;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		txtFieldId.setEnabled(false);
		btnVerify.setEnabled(false);
		btnValidate.setEnabled(true);
	}
}
