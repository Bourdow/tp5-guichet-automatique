package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import controller.WithdrawMoneyController;
import model.User;
import view.WithdrawPanel;

public class WithdrawMoneyAction implements ActionListener{
	
	JFrame mainFrame;
	User user;
	public WithdrawMoneyAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mainFrame.setTitle("Le Guichet -- Withdraw");
		WithdrawPanel withdrawPanel = new WithdrawPanel(mainFrame, new WithdrawMoneyController(user));
		mainFrame.add(withdrawPanel);
		mainFrame.setContentPane(withdrawPanel);
		mainFrame.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}
