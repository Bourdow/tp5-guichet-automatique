package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import controller.MainController;
import model.User;
import view.HomePanel;

public class EnglishRadioAction implements ActionListener {

	JFrame mainFrame;
	User user;
	
	public EnglishRadioAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		user.setLanguage("en");
		user.setCountry("US");
		//mainFrame.setTitle("Le Guichet -- Home");
		HomePanel panel = new HomePanel(mainFrame, new MainController(user));
		mainFrame.setContentPane(panel);
		panel.getController().getUser().update();
		panel.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}