package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.User;

public class RegisterAction implements ActionListener{

	JFrame mainFrame = new JFrame();

	public RegisterAction(JFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Move to external view + Send JOptionPane Class
		JLabel labelFirstName = new JLabel("First Name"); final JTextField firstname = new JTextField();
		JLabel labelLastName = new JLabel("Last Name"); final JTextField lastname = new JTextField();
		JLabel labelUser = new JLabel("Username"); final JTextField username = new JTextField();
		JLabel labelPassword = new JLabel("Password"); final JPasswordField password = new JPasswordField();
		Object[] tab = new Object[] { labelFirstName, firstname, labelLastName, lastname, labelUser, username,
				labelPassword, password};
		final JOptionPane jop = new JOptionPane(tab, JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		final JDialog dialog = new JDialog(mainFrame, "Register", true);
		dialog.setContentPane(jop);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		    	dialog.setVisible(false);
		    }
		});
	
		jop.addPropertyChangeListener(
		    new PropertyChangeListener() {
		    	private boolean bool = true;
		        public void propertyChange(PropertyChangeEvent e) {
		        	if(bool) {
			            String prop = e.getPropertyName();
			            if (dialog.isVisible()  && (e.getSource() == jop) && prop.equals(JOptionPane.VALUE_PROPERTY)) {	 
			            	int rep = ((Integer) jop.getValue()).intValue();
			        		if (rep == JOptionPane.OK_OPTION){
			        			if(!User.isUsernameTaken(username.getText())) {
			        				User u = new User(firstname.getText(), lastname.getText(), username.getText(), String.valueOf(password.getPassword()), "en", "US");
			        				User.addUser(u);
			        				JOptionPane.showMessageDialog(mainFrame, "User successfully created !", "Info", JOptionPane.PLAIN_MESSAGE);
			        				dialog.setVisible(false);
			        			}else{
			        				JOptionPane.showMessageDialog(mainFrame, "Username already exists", "Error", JOptionPane.ERROR_MESSAGE);
			        			}
			        		}else{
			        			dialog.setVisible(false);
			        		}
			            }
			            bool = false;
			            jop.setValue(JOptionPane.UNINITIALIZED_VALUE);
			            bool = true;
		        	}
		        }
		    }
		);
		dialog.pack();
		dialog.setVisible(true);
	}
}
