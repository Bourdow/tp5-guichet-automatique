package controller.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import view.AuthPanel;

public class LogOutAction implements MouseListener{
	
	JFrame mainFrame;
	view.MenuBar menuBar;
	
	public LogOutAction(JFrame mainFrame, view.MenuBar menuBar) {
		this.mainFrame = mainFrame;
		this.menuBar = menuBar;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		mainFrame.setTitle("Le Guichet -- LogIn");
		AuthPanel panel = new AuthPanel(mainFrame);
		mainFrame.setContentPane(panel);
		panel.setVisible(true);
		menuBar.setVisible(false);
		mainFrame.repaint();
		mainFrame.revalidate();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
