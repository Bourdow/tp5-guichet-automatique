package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

public class JoinRadioAction implements ActionListener {

	JButton btnValidate;
	JTextField txtFieldId;
	JButton btnVerify;
	
	public JoinRadioAction(JTextField txtFieldId, JButton btnVerify, JButton btnValidate) {
		this.txtFieldId = txtFieldId;
		this.btnVerify = btnVerify;
		this.btnValidate = btnValidate;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		txtFieldId.setEnabled(true);
		btnVerify.setEnabled(true);
		btnValidate.setEnabled(false);
	}
}
