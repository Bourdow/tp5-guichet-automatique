package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.AddAccountController;
import model.User;
import view.AddAccountPanel;
import view.BalancePanel;

public class AddAccountAction implements ActionListener{

	JFrame mainFrame = new JFrame();
	User user;

	public AddAccountAction(JFrame mainFrame, User user) {
		super();
		this.mainFrame = mainFrame;
		this.user = user;
	}

	public void actionPerformed(ActionEvent e) {
		mainFrame.setTitle("Le Guichet -- Add Account");
		AddAccountPanel addAccountPanel = new AddAccountPanel(mainFrame, new AddAccountController(user));
		mainFrame.add(addAccountPanel);
		mainFrame.setContentPane(addAccountPanel);
		mainFrame.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}
