package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controller.AddAccountController;
import controller.MainController;
import model.Account;
import model.User;
import view.HomePanel;

public class ValidateAccountAction implements ActionListener {

	JFrame mainFrame;
	JRadioButton joinRadioButton;
	AddAccountController controller;

	public ValidateAccountAction(JFrame mainFrame, JRadioButton joinRadioButton, AddAccountController controller) {
		this.mainFrame = mainFrame;
		this.joinRadioButton = joinRadioButton;
		this.controller = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!joinRadioButton.isSelected())
		{
			List<Integer> list = new ArrayList<>();
			list.add(controller.getUser().getId());
			Account a = new Account(list, 1, 0d, 200d);
			Account.addAccount(a);
			JOptionPane.showMessageDialog(mainFrame, "Account successfully created !", "Info", JOptionPane.PLAIN_MESSAGE);
			goToHomePanel();
		}
		else
		{
			List<Integer> list = new ArrayList<>();
			list.add(controller.getUser().getId());
			list.add(controller.getValidatedId());
			Account a = new Account(list, 2, 0d, 200d);
			boolean accountCreated = Account.addAccount(a);
			if(accountCreated) {
				JOptionPane.showMessageDialog(mainFrame, "Account successfully created !", "Info", JOptionPane.PLAIN_MESSAGE);
				goToHomePanel();
			}else {
				JOptionPane.showMessageDialog(mainFrame, "One of the users have reached the max account limit !", "Error", JOptionPane.PLAIN_MESSAGE);
			}
		}
		
	}

	private void goToHomePanel() {
		mainFrame.setTitle("Le Guichet -- Home");
		HomePanel panel = new HomePanel(mainFrame, new MainController(controller.getUser()));
		mainFrame.setContentPane(panel);
		panel.getController().getUser().refresh();
		panel.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}
