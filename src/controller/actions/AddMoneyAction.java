package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.AddMoneyController;
import model.User;
import view.AddMoneyPanel;

public class AddMoneyAction implements ActionListener{

	JFrame mainFrame;
	User user;

	public AddMoneyAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mainFrame.setTitle("Le Guichet -- Credit");
		AddMoneyPanel panel = new AddMoneyPanel(mainFrame, new AddMoneyController(user));
		mainFrame.add(panel);
		mainFrame.setContentPane(panel);
		mainFrame.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
	
}
