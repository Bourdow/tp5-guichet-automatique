package controller.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import controller.ProfileController;
import model.User;
import view.ProfilePanel;

public class GoToProfilAction implements MouseListener{
	JFrame mainFrame;
	User user;

	public GoToProfilAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		mainFrame.setTitle("Le Guichet -- Profile");
		ProfilePanel panel = new ProfilePanel(mainFrame, new ProfileController(user));
		mainFrame.setContentPane(panel);
		panel.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}