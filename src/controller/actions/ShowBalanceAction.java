package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import controller.ShowBalanceController;
import model.User;
import view.BalancePanel;


public class ShowBalanceAction implements ActionListener{

	JFrame mainFrame;
	User user;
	public ShowBalanceAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mainFrame.setTitle("Le Guichet -- Balance");
		BalancePanel balancePanel = new BalancePanel(mainFrame, new ShowBalanceController(user));
		mainFrame.add(balancePanel);
		mainFrame.setContentPane(balancePanel);
		mainFrame.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}
}
