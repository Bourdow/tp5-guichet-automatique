package controller.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AddAccountController;
import model.User;

public class VerifyAction  implements ActionListener{

	JTextField idField;
	JLabel label;
	JButton validationButton;
	AddAccountController controller;
	
	public VerifyAction(JTextField idField, JLabel label, JButton validationButton, AddAccountController controller) {
		this.idField = idField;
		this.label = label;
		this.validationButton = validationButton;
		this.controller = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		User u = User.getUserById(Integer.parseInt(idField.getText()));
		if (null != u)
		{
			label.setText("The user exist ! You can create an account with him/her UwU");
			label.setForeground(Color.green);
			label.setVisible(true);
			validationButton.setEnabled(true);
			controller.setValidatedId(Integer.parseInt(idField.getText()));
		}
		else
		{
			label.setText("The user doesn't exist ! Make some real friends pls");
			label.setForeground(Color.red);
			label.setVisible(true);
			validationButton.setEnabled(false);
			controller.setValidatedId(0);
		}
	}
}
