package controller.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import controller.MainController;
import model.User;
import view.HomePanel;

public class BackHomeAction implements MouseListener{
	
	JFrame mainFrame;
	User user;
	
	public BackHomeAction(JFrame mainFrame, User user) {
		this.mainFrame = mainFrame;
		this.user = user;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		mainFrame.setTitle("Le Guichet -- Home");
		HomePanel panel = new HomePanel(mainFrame, new MainController(user));
		mainFrame.setContentPane(panel);
		panel.getController().getUser().refresh();
		panel.setVisible(true);
		mainFrame.repaint();
		mainFrame.revalidate();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}