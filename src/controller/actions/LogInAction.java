package controller.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.MainController;
import model.User;
import view.AuthPanel;
import view.HomePanel;
import view.MenuBar;


public class LogInAction implements ActionListener{

	JFrame mainFrame = new JFrame();
	JTextField usernameField;
	JTextField pwdField;
	JLabel label;
	
	public LogInAction(JFrame mainFrame,JTextField usernameField, JTextField pwdField, JLabel label) {
		this.mainFrame = mainFrame;
		this.usernameField = usernameField;
		this.pwdField = pwdField;
		this.label = label;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		User u = User.auth(usernameField.getText(), pwdField.getText());
		if(null != u) {
			mainFrame.setTitle("Le Guichet -- Home");
			HomePanel hp = new HomePanel(mainFrame, new MainController(u));
			mainFrame.setJMenuBar(new MenuBar(mainFrame, new MainController(u)));
			mainFrame.add(hp);
			mainFrame.setContentPane(hp);
			mainFrame.repaint();
			mainFrame.revalidate();
		}
		else
		{
			label.setVisible(true);
		}
	}
}
