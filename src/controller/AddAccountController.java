package controller;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import model.Account;
import model.User;

public class AddAccountController {
	
	User user;
	List<Account> accounts;
	int validatedId;
	private ResourceBundle messages;
	
	public AddAccountController(User user) {
		this.setUser(user);
		this.setAccounts(user.getAccounts());
		Locale currentLocale = new Locale(user.getLanguage(), user.getCountry());
		this.messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public int getValidatedId() {
		return validatedId;
	}

	public void setValidatedId(int validatedId) {
		this.validatedId = validatedId;
	}
	
	public ResourceBundle getMessages(){
		  return this.messages;
		}
}
