package controller;

import java.util.Locale;
import java.util.ResourceBundle;

import model.User;

public class MainController {
	
	private User user;
	private ResourceBundle messages;
	
	public MainController(User user) {
		this.setUser(user);
		Locale currentLocale = new Locale(user.getLanguage(), user.getCountry());
		this.messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ResourceBundle getMessages(){
		  return this.messages;
		}
}
