package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import controller.MainController;
import controller.actions.AddAccountAction;
import controller.actions.AddMoneyAction;
import controller.actions.EnglishRadioAction;
import controller.actions.FrenchRadioAction;
import controller.actions.ShowBalanceAction;
import controller.actions.WithdrawMoneyAction;

public class HomePanel extends JPanel{
	
	public MainController controller;
	
	public HomePanel(JFrame mainFrame, MainController controller)
	{		
		this.controller = controller;
		this.setLayout(new BorderLayout());

		JPanel panelBtnHome = new JPanel();
		panelBtnHome.setLayout(new GridLayout(2, 2));
		JPanel panelbtnMain = new JPanel();
		panelbtnMain.setLayout(new BorderLayout());

		JPanel panelbtnAddAccount = new JPanel();
		panelbtnAddAccount.setLayout(new BorderLayout());
		JPanel panelbtnAddMoney = new JPanel();
		panelbtnAddMoney.setLayout(new BorderLayout());
		JPanel panelbtnWithdrawMoney = new JPanel();
		panelbtnWithdrawMoney.setLayout(new BorderLayout());
		JPanel panelbtnShowBalance = new JPanel();
		panelbtnShowBalance.setLayout(new BorderLayout());

		JPanel panelNorth;
		JPanel panelSouth;
		JPanel panelEast;
		JPanel panelWest;

		JPanel langagePanel = new JPanel();
		
		JRadioButton frenchRadioButton = new JRadioButton("fr");
		JRadioButton englishRadioButton = new JRadioButton("en");
		
		if(getController().getUser().getLanguage() == "fr")
		{
			frenchRadioButton.setSelected(true);
		}
		else
		{
			englishRadioButton.setSelected(true);
		}
		
		ButtonGroup group = new ButtonGroup();
		group.add(frenchRadioButton);
		group.add(englishRadioButton);
		
		JLabel labelWelcome = new JLabel(getController().getMessages().getString("welcome") + " " +controller.getUser().getFirstName() + " !", SwingConstants.CENTER);

		JButton btnAddAccount = new JButton(getController().getMessages().getString("label_create_account"));
		JButton btnAddMoney = new JButton(getController().getMessages().getString("label_deposit_money"));
		JButton btnWithdrawMoney = new JButton(getController().getMessages().getString("label_withdraw_money"));
		JButton btnShowBalance = new JButton(getController().getMessages().getString("label_show_balance"));

		panelbtnAddAccount.add(panelNorth = new JPanel(), BorderLayout.NORTH);
		panelbtnAddAccount.add(panelSouth = new JPanel(), BorderLayout.SOUTH);
		panelbtnAddAccount.add(panelEast = new JPanel(), BorderLayout.EAST);
		panelbtnAddAccount.add(panelWest = new JPanel(), BorderLayout.WEST);
		panelbtnAddMoney.add(panelNorth = new JPanel(), BorderLayout.NORTH);
		panelbtnAddMoney.add(panelSouth = new JPanel(), BorderLayout.SOUTH);
		panelbtnAddMoney.add(panelEast = new JPanel(), BorderLayout.EAST);
		panelbtnAddMoney.add(panelWest = new JPanel(), BorderLayout.WEST);
		panelbtnWithdrawMoney.add(panelNorth = new JPanel(), BorderLayout.NORTH);
		panelbtnWithdrawMoney.add(panelSouth = new JPanel(), BorderLayout.SOUTH);
		panelbtnWithdrawMoney.add(panelEast = new JPanel(), BorderLayout.EAST);
		panelbtnWithdrawMoney.add(panelWest = new JPanel(), BorderLayout.WEST);
		panelbtnShowBalance.add(panelNorth = new JPanel(), BorderLayout.NORTH);
		panelbtnShowBalance.add(panelSouth = new JPanel(), BorderLayout.SOUTH);
		panelbtnShowBalance.add(panelEast = new JPanel(), BorderLayout.EAST);
		panelbtnShowBalance.add(panelWest = new JPanel(), BorderLayout.WEST);


		panelbtnShowBalance.add(btnShowBalance, BorderLayout.CENTER);
		panelbtnAddMoney.add(btnAddMoney, BorderLayout.CENTER);
		panelbtnWithdrawMoney.add(btnWithdrawMoney, BorderLayout.CENTER);
		panelbtnAddAccount.add(btnAddAccount, BorderLayout.CENTER);

		panelBtnHome.add(panelbtnShowBalance);
		panelBtnHome.add(panelbtnAddMoney);
		panelBtnHome.add(panelbtnWithdrawMoney);
		panelBtnHome.add(panelbtnAddAccount);

		langagePanel.add(englishRadioButton);
		langagePanel.add(labelWelcome);
		langagePanel.add(frenchRadioButton);
		panelbtnMain.add(langagePanel, BorderLayout.NORTH);
		panelbtnMain.add(panelSouth = new JPanel(), BorderLayout.SOUTH);
		panelbtnMain.add(panelEast = new JPanel(), BorderLayout.EAST);
		panelbtnMain.add(panelWest = new JPanel(), BorderLayout.WEST);
		panelbtnMain.add(panelBtnHome, BorderLayout.CENTER);

		this.add(panelbtnMain, BorderLayout.CENTER);
		
		// Action
		
		btnShowBalance.addActionListener(new ShowBalanceAction(mainFrame, getController().getUser()));
		
		btnAddMoney.addActionListener(new AddMoneyAction(mainFrame, getController().getUser()));
		
		btnWithdrawMoney.addActionListener(new WithdrawMoneyAction(mainFrame, getController().getUser()));
		
		if(getController().getUser().accounts.size() <2) {
			btnAddAccount.setEnabled(true);
			btnAddAccount.addActionListener(new AddAccountAction(mainFrame,  getController().getUser()));
		}else {
			btnAddAccount.setEnabled(false);
		}
		
		frenchRadioButton.addActionListener(new FrenchRadioAction(mainFrame, getController().getUser()));
		
		englishRadioButton.addActionListener(new EnglishRadioAction(mainFrame, getController().getUser()));
	}

	public MainController getController() {
		return controller;
	}
	
	
}
