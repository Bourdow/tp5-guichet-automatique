package view;

import javax.swing.JFrame;


public class Main {

	public static void main(String[] args) {
		
		JFrame mainFrame = new JFrame("Le Guichet");
		
		AuthPanel AuthPanel = new AuthPanel(mainFrame);

		mainFrame.add(AuthPanel);
		
		mainFrame.setVisible(true);

		mainFrame.setResizable(false);

		mainFrame.setContentPane(AuthPanel);

		mainFrame.setSize(550, 300);

		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}