package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.AddAccountController;
import controller.AddMoneyController;
import controller.actions.CourantRadioAction;
import controller.actions.JoinRadioAction;
import controller.actions.ValidateAccountAction;
import controller.actions.VerifyAction;
import model.User;

public class AddAccountPanel extends JPanel{
	
	private AddAccountController controller;

	public AddAccountPanel(JFrame mainFrame, AddAccountController controller) {

		this.controller = controller;
		this.setLayout(new BorderLayout());

		JPanel ColumnPanel  = new JPanel();
		ColumnPanel.setLayout(new GridLayout(5,1));

		JPanel labelWelcomePanel = new JPanel();
		labelWelcomePanel.setLayout(new BorderLayout());

		JLabel welcomeLabel = new JLabel(getController().getMessages().getString("label_create_account"), SwingConstants.CENTER);

		JPanel emptyPanel;

		JPanel choiceTypeAccountPanel = new JPanel();

		JRadioButton joinRadioButton = new JRadioButton(getController().getMessages().getString("joint"));
		joinRadioButton.setSelected(true);

		JRadioButton courantRadioButton = new JRadioButton(getController().getMessages().getString("courant"));

		ButtonGroup group = new ButtonGroup();
		group.add(joinRadioButton);
		group.add(courantRadioButton);

		JLabel typeAccountLabel = new JLabel(getController().getMessages().getString("label_account_type") + " :");

		JPanel secondOwnerPanel = new JPanel();

		JLabel secondOwnerLabel = new JLabel(getController().getMessages().getString("label_coowner_id") + " :");

		JTextField secondOwnerTextField = new JTextField();
		secondOwnerTextField.setColumns(12);

		JButton secondOwnerButton = new JButton(getController().getMessages().getString("verify"));

		JPanel secondOwnerCheckPanel = new JPanel();

		JLabel secondOwnerCheckLabel = new JLabel("", SwingConstants.CENTER);

		JPanel validationPanel = new JPanel();

		JButton validationButton = new JButton(getController().getMessages().getString("label_create_account"));
		validationButton.setEnabled(false);

		labelWelcomePanel.add(welcomeLabel, BorderLayout.CENTER);

		choiceTypeAccountPanel.add(typeAccountLabel);
		choiceTypeAccountPanel.add(joinRadioButton);
		choiceTypeAccountPanel.add(courantRadioButton);

		secondOwnerPanel.add(secondOwnerLabel);
		secondOwnerPanel.add(secondOwnerTextField);
		secondOwnerPanel.add(secondOwnerButton);

		secondOwnerCheckPanel.add(secondOwnerCheckLabel);

		validationPanel.add(validationButton);

		ColumnPanel.add(labelWelcomePanel);
		ColumnPanel.add(choiceTypeAccountPanel);
		ColumnPanel.add(secondOwnerPanel);
		ColumnPanel.add(secondOwnerCheckLabel);
		ColumnPanel.add(validationPanel);

		this.add(ColumnPanel);

		// Action

		joinRadioButton.addActionListener(new JoinRadioAction(secondOwnerTextField, secondOwnerButton, validationButton));

		courantRadioButton.addActionListener(new CourantRadioAction(secondOwnerTextField, secondOwnerButton, validationButton));

		secondOwnerButton.addActionListener(new VerifyAction(secondOwnerTextField, secondOwnerCheckLabel, validationButton, getController()));
	
		validationButton.addActionListener(new ValidateAccountAction(mainFrame, joinRadioButton, getController()));
	}
	
	public AddAccountController getController() {
		return controller;
	}	
}