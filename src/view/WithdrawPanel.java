package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.MainController;
import controller.WithdrawMoneyController;
import model.Account;

public class WithdrawPanel extends JPanel{

	private WithdrawMoneyController controller;
	JComboBox<Integer> choiceAccountCombo;
	JTextField amountTransactionTextField;
	private JFrame mainFrame;
	
	public WithdrawPanel(JFrame mainFrame, WithdrawMoneyController controller) {
		this.controller = controller;
		this.mainFrame = mainFrame;
		this.setLayout(new BorderLayout());

		JPanel ColumnPanel  = new JPanel();
		ColumnPanel.setLayout(new GridLayout(6,1));
		
		JPanel labelWelcomePanel = new JPanel();
		labelWelcomePanel.setLayout(new BorderLayout());
		
		JLabel welcomeLabel = new JLabel(getController().getMessages().getString("label_withdraw_money"), SwingConstants.CENTER);

		JPanel choiceAccountPanel = new JPanel();
		
		JLabel choiceAccountLabel = new JLabel(getController().getMessages().getString("label_account_to_debit"));
		
		choiceAccountCombo = new JComboBox<Integer>();
		for(Account acc : this.getController().getUser().getAccounts()) {
			choiceAccountCombo.addItem(acc.getId());
		}
		JPanel amountTransactionPanel = new JPanel();
		
		JLabel amountTransactionLabel = new JLabel(getController().getMessages().getString("amount")+" :");
		
		amountTransactionTextField = new JTextField();
		amountTransactionTextField.setColumns(12);
		
		JLabel eurosTransactionLabel = new JLabel("Euros Dollarz");
		
		JPanel validationPanel = new JPanel();
		
		JButton validationButton = new JButton(getController().getMessages().getString("label_validate_transaction"));
		
		labelWelcomePanel.add(welcomeLabel, BorderLayout.CENTER);
		
		choiceAccountPanel.add(choiceAccountLabel);
		choiceAccountPanel.add(choiceAccountCombo);

		amountTransactionPanel.add(amountTransactionLabel);
		amountTransactionPanel.add(amountTransactionTextField);
		amountTransactionPanel.add(eurosTransactionLabel);
		
		validationPanel.add(validationButton);
		
		validationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Double montant = Double.parseDouble(amountTransactionTextField.getText());
				for(Account acc : getController().getAccounts()) { 
				   if(acc.getId() == (Integer) choiceAccountCombo.getSelectedItem()) { 
					   acc.withdraw(getController().getUser().getId(), montant);
				   }
				}
				getMainFrame().setTitle(getController().getMessages().getString("title_home"));
				HomePanel panel = new HomePanel(getMainFrame(), new MainController(getController().getUser()));
				getMainFrame().setContentPane(panel);
				panel.getController().getUser().refresh();
				panel.setVisible(true);
				getMainFrame().repaint();
				getMainFrame().revalidate();
			}
		});
		
		ColumnPanel.add(labelWelcomePanel);
		ColumnPanel.add(new JPanel());
		ColumnPanel.add(choiceAccountPanel);
		ColumnPanel.add(amountTransactionPanel);
		ColumnPanel.add(new JPanel());
		ColumnPanel.add(validationPanel);
	
		this.add(ColumnPanel);
	}
	
	public WithdrawMoneyController getController() {
		return controller;
	}
	
	public JFrame getMainFrame() {
		return mainFrame;
	}
}
