package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.ShowBalanceController;
import model.Account;
import model.Transaction;

public class BalancePanel extends JPanel{

	private ShowBalanceController controller;
	private DefaultTableModel model;
	JComboBox<Integer> comboAccountBalance;
	JLabel labelAmountBalanceNumber;
	Account currentAccountSelected;
	
	public BalancePanel(JFrame mainFrame, ShowBalanceController controller) {
		this.controller = controller;
		this.setLayout(new BorderLayout());

		JPanel panelLastTransaction = new JPanel();

		JPanel panelGridRight = new JPanel();
		panelGridRight.setLayout(new GridLayout(4,1));

		JPanel panelAmountBalance = new JPanel();
		panelAmountBalance.setLayout(new GridLayout(2,1));
		
		JPanel panelGridAccountBalance = new JPanel();
		panelGridAccountBalance.setLayout(new GridLayout(2,1));
		
		JLabel labelUserBalance = new JLabel(getController().getMessages().getString("user") + " : ");
		comboAccountBalance = new JComboBox<Integer>();
		boolean first = true;
		for(Account acc : controller.getAccounts()) {
			if(first) {
				currentAccountSelected = acc;
				first = !first;
			}
			comboAccountBalance.addItem(acc.getId());
		}
		JButton btnValidateBalance = new JButton(getController().getMessages().getString("validate"));
		JLabel labelAmountBalance = new JLabel(getController().getMessages().getString("label_your_balance") + " :");
		labelAmountBalanceNumber = new JLabel();

		model = new DefaultTableModel();
		model.addColumn(getController().getMessages().getString("date"));
		model.addColumn(getController().getMessages().getString("type"));
		model.addColumn(getController().getMessages().getString("amount"));
		model.addColumn(getController().getMessages().getString("by"));
		JTable table = new JTable(model);
		JScrollPane scroll = new JScrollPane(table);
		
		panelLastTransaction.add(scroll, BorderLayout.CENTER);
		
		panelGridAccountBalance.add(comboAccountBalance);
		panelGridAccountBalance.add(btnValidateBalance);
		panelGridRight.add(labelUserBalance);
		panelGridRight.add(panelGridAccountBalance);
		panelAmountBalance.add(labelAmountBalance);
		panelAmountBalance.add(labelAmountBalanceNumber);
		panelGridRight.add(panelAmountBalance);

		btnValidateBalance.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for(Account acc : getController().getAccounts()) { 
				   if(acc.getId() == (Integer) comboAccountBalance.getSelectedItem()) { 
					   currentAccountSelected = acc;
					   getTableModel().setRowCount(0);
					   for(Transaction t : acc.getLast5Transactions()) {
							Object[] obj = {t.getDate(), t.getType(), t.getAmount(), t.getOwner().getUsername()};
							getTableModel().addRow(obj);
						}
					   labelAmountBalanceNumber.setText(Double.toString(acc.getAmount()) + " $$");
				   }
				}
			}
		});
		this.add(panelLastTransaction);
		this.add(panelGridRight, BorderLayout.EAST);
	}

	public ShowBalanceController getController() {
		return controller;
	}
	
	public DefaultTableModel getTableModel() {
		return model;
	}
}
