package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.actions.LogInAction;
import controller.actions.RegisterAction;

public class AuthPanel extends JPanel{

	public AuthPanel(JFrame mainFrame) {

		this.setLayout(new BorderLayout());

		JPanel panelHeader = new JPanel();
		panelHeader.setLayout(new GridLayout(2,1));
		JPanel panelMid = new JPanel();
		panelMid.setLayout(new GridLayout(1,3));
		JPanel panelBtn = new JPanel();
		panelBtn.setLayout(new GridLayout(2,3));

		JPanel panelLeft = new JPanel();
		JPanel panelCenter = new JPanel();
		panelCenter.setLayout(new GridLayout(5,1));
		JPanel panelRight = new JPanel();

		JLabel labelPresAuth = new JLabel("Welcome, please login", SwingConstants.CENTER);
		JLabel labelUserAuth = new JLabel("Username");
		JLabel labelPasswordAuth = new JLabel("Password");	
		JLabel labelErrorAuth = new JLabel("Wrong User or Password, try again", SwingConstants.CENTER);
		labelErrorAuth.setForeground(Color.red);
		labelErrorAuth.setVisible(false);
		JLabel labelSautdeLigne = new JLabel();

		JTextField textFieldUserAuth = new JTextField();
		JPasswordField textFieldPassAuth = new  JPasswordField();
		textFieldPassAuth.setColumns(12);
		textFieldUserAuth.setColumns(12);

		JButton btnLogIn = new JButton("Log In");
		JButton btnRegister = new JButton("Register");

		panelHeader.add(labelPresAuth);
		panelHeader.add(labelErrorAuth);

		panelCenter.add(labelUserAuth);
		panelCenter.add(textFieldUserAuth);
		panelCenter.add(labelPasswordAuth);
		panelCenter.add(textFieldPassAuth);
		panelCenter.add(labelSautdeLigne);

		panelMid.add(panelLeft);
		panelMid.add(panelCenter);
		panelMid.add(panelRight);

		panelBtn.add(new JPanel());
		panelBtn.add(btnRegister);
		panelBtn.add(new JPanel());
		panelBtn.add(new JPanel());
		panelBtn.add(btnLogIn);
		panelBtn.add(new JPanel());

		this.add(panelHeader, BorderLayout.NORTH);
		this.add(panelMid, BorderLayout.CENTER);
		this.add(panelBtn, BorderLayout.SOUTH);

		// button action
		btnRegister.addActionListener(new RegisterAction(mainFrame));

		//Action LogIn
		btnLogIn.addActionListener(new LogInAction(mainFrame, textFieldUserAuth, textFieldPassAuth, labelErrorAuth));
	}
}
