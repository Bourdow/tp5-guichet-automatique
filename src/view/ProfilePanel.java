package view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.MainController;
import controller.ProfileController;
import controller.actions.ModifyProfileAction;
import model.Account;
import model.User;

public class ProfilePanel extends JPanel{

	private ProfileController controller;

	public ProfilePanel(JFrame mainFrame, ProfileController controller)
	{
		this.controller = controller;

		this.setLayout(new GridLayout(1, 2));

		JPanel profileInfoPanel = new JPanel();
		profileInfoPanel.setLayout(new GridLayout(11, 1));

		JLabel userIdLabel = new JLabel(getController().getMessages().getString("user") + " " + getController().getMessages().getString("id") + " : ", SwingConstants.CENTER);
		JLabel userIdLabel2 = new JLabel(Integer.toString(getController().getUser().getId()), SwingConstants.CENTER);

		JLabel userUsernameLabel = new JLabel(getController().getMessages().getString("user") + " " + getController().getMessages().getString("label_username") + " : ", SwingConstants.CENTER);
		JLabel userUsernameLabel2 = new JLabel(getController().getUser().getUsername(), SwingConstants.CENTER);

		JLabel userPasswordLabel = new JLabel(getController().getMessages().getString("user") + " " + getController().getMessages().getString("label_password") + " : ", SwingConstants.CENTER);
		JPasswordField userPasswordTextField = new JPasswordField(getController().getUser().getPassword(), SwingConstants.CENTER);

		JLabel userNameLabel = new JLabel(getController().getMessages().getString("user") + " " + getController().getMessages().getString("label_firstname") + " : ", SwingConstants.CENTER);
		JTextField userNameTextField = new JTextField(getController().getUser().getFirstName(), SwingConstants.CENTER);

		JLabel userLastNameLabel = new JLabel(getController().getMessages().getString("user") + " " + getController().getMessages().getString("label_lastname") + " : ", SwingConstants.CENTER);
		JTextField userLastNameTextField = new JTextField(getController().getUser().getLastName(), SwingConstants.CENTER);

		JButton userProfileCheck = new JButton(getController().getMessages().getString("label_save_changes"));

		profileInfoPanel.add(userIdLabel);
		profileInfoPanel.add(userIdLabel2);
		profileInfoPanel.add(userUsernameLabel);
		profileInfoPanel.add(userUsernameLabel2);
		profileInfoPanel.add(userPasswordLabel);
		profileInfoPanel.add(userPasswordTextField);
		profileInfoPanel.add(userNameLabel);
		profileInfoPanel.add(userNameTextField);
		profileInfoPanel.add(userLastNameLabel);
		profileInfoPanel.add(userLastNameTextField);
		profileInfoPanel.add(userProfileCheck);

		JPanel profileAccountPanel = new JPanel();
		profileAccountPanel.setLayout(new GridLayout(14, 1));

		JLabel account1Label = new JLabel(getController().getMessages().getString("account") + " 1 : ", SwingConstants.CENTER);

		JLabel account1IdLabel = new JLabel(getController().getMessages().getString("id")+ " : ", SwingConstants.CENTER);
		JLabel account1IdLabel2 = new JLabel("", SwingConstants.CENTER);

		JLabel account1TypeLabel = new JLabel(getController().getMessages().getString("type")+ " : ", SwingConstants.CENTER);
		JLabel account1TypeLabel2 = new JLabel("", SwingConstants.CENTER);

		JLabel account1OwnersLabel = new JLabel(getController().getMessages().getString("owners")+ " : ", SwingConstants.CENTER);
		JLabel account1OwnersLabel2 = new JLabel("", SwingConstants.CENTER);

		JLabel account2Label = new JLabel(getController().getMessages().getString("account") + " 2 : ", SwingConstants.CENTER);

		JLabel account2IdLabel = new JLabel(getController().getMessages().getString("id")+ " : ", SwingConstants.CENTER);
		JLabel account2IdLabel2 = new JLabel("", SwingConstants.CENTER);

		JLabel account2TypeLabel = new JLabel(getController().getMessages().getString("type")+ " : ", SwingConstants.CENTER);
		JLabel account2TypeLabel2 = new JLabel("", SwingConstants.CENTER);

		JLabel account2OwnersLabel = new JLabel(getController().getMessages().getString("owners")+ " : ", SwingConstants.CENTER);
		JLabel account2OwnersLabel2 = new JLabel("", SwingConstants.CENTER);

		if(getController().getAccounts().size() >= 1)
		{
			Account acc = getController().getAccounts().get(0);
			account1IdLabel2.setText(Integer.toString(acc.getId()));
			account1TypeLabel2.setText(acc.getType().toString());
			String accu = "";
			for (User i : acc.getOwners())
			{
				accu += " " + i.getUsername() + " ";
			}
			account1OwnersLabel2.setText(accu);
		}
		if(getController().getAccounts().size() >= 2)
		{
			Account acc = getController().getAccounts().get(1);
			account2IdLabel2.setText(Integer.toString(acc.getId()));
			account2TypeLabel2.setText(acc.getType().toString());
			String accu = "";
			for (User i : acc.getOwners())
			{
				accu += " " + i.getUsername() + " ";
			}
			account2OwnersLabel2.setText(accu);
		}
		
		profileAccountPanel.add(account1Label);
		profileAccountPanel.add(account1IdLabel);
		profileAccountPanel.add(account1IdLabel2);
		profileAccountPanel.add(account1TypeLabel);
		profileAccountPanel.add(account1TypeLabel2);
		profileAccountPanel.add(account1OwnersLabel);
		profileAccountPanel.add(account1OwnersLabel2);
		profileAccountPanel.add(account2Label);
		profileAccountPanel.add(account2IdLabel);
		profileAccountPanel.add(account2IdLabel2);
		profileAccountPanel.add(account2TypeLabel);
		profileAccountPanel.add(account2TypeLabel2);
		profileAccountPanel.add(account2OwnersLabel);
		profileAccountPanel.add(account2OwnersLabel2);

		this.add(profileInfoPanel);
		this.add(profileAccountPanel);

		userProfileCheck.addActionListener(new ModifyProfileAction(mainFrame, getController().getUser(), userPasswordTextField, userNameTextField, userLastNameTextField));
	}

	public ProfileController getController() {
		return controller;
	}	
}
