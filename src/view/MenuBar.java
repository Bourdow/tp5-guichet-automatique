package view;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import controller.MainController;
import controller.actions.BackHomeAction;
import controller.actions.GoToProfilAction;
import controller.actions.LogOutAction;

public class MenuBar extends JMenuBar{
	
	private MainController controller;

	public MenuBar(JFrame mainFrame, MainController controller) { 
		
		this.controller = controller;
		
		JMenu menuHome = new JMenu(getController().getMessages().getString("home"));
		JMenu menuAccount = new JMenu(getController().getMessages().getString("profile"));
		JMenu menuLogOut = new JMenu(getController().getMessages().getString("logout"));

		this.add(menuHome);
		this.add(menuAccount);
		this.add(menuLogOut);
		
		menuHome.addMouseListener(new BackHomeAction(mainFrame, getController().getUser()));
		menuAccount.addMouseListener(new GoToProfilAction(mainFrame, getController().getUser()));
		menuLogOut.addMouseListener(new LogOutAction(mainFrame, this));
	}
	
	public MainController getController() {
		return controller;
	}	
}
