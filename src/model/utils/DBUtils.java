package model.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import model.Account;
import model.Transaction;
import model.User;

public class DBUtils {

	private static String driver = "org.postgresql.Driver";
	private static final Logger LOGGER = (Logger) LogManager.getLogger(DBUtils.class);

	public static User toUser(ResultSet res) {
		try {
			if (res.next()) {
				int id = res.getInt(1);
				String firstName = res.getString(2);
				String lastName = res.getString(3);
				String username = res.getString(4);
				String password = res.getString(5);
				String language = res.getString(6);
				String country = res.getString(7);
				List<Integer> accounts = new ArrayList<>();
				Connection con = DBUtils.getConnection();
				Statement st = con.createStatement();
				ResultSet resAccounts = st.executeQuery("SELECT id_account FROM owns WHERE id_user=" + id);
				while (resAccounts.next()) {
					accounts.add(resAccounts.getInt(1));
				}
				return new User(id, firstName, lastName, username, password, accounts, language, country);
			}
			return null;
		} catch (SQLException e) {
			LOGGER.error("Could not create User object :", e);
			return null;
		}
	}

	public static Account toAccount(ResultSet res) {
		try {
			res.next();
			int id = res.getInt(1);
			int type = res.getInt(2);
			double amount = res.getDouble(3);
			double overdraft = res.getDouble(4);
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			// List of owners
			ResultSet resOwners = st.executeQuery("SELECT id_user FROM owns WHERE id_account=" + id);
			List<Integer> owners = new ArrayList<>();
			while (resOwners.next()) {
				owners.add(resOwners.getInt(1));
			}
			// List of transactions
			ResultSet resTransactions = st.executeQuery("SELECT id FROM transactions WHERE account=" + id);
			List<Integer> transactions = new ArrayList<>();
			while (resTransactions.next()) {
				transactions.add(resTransactions.getInt(1));
			}
			return new Account(id, owners, type, amount, overdraft, transactions);
		} catch (SQLException e) {
			LOGGER.error("Could not create Account object :", e);
			return null;
		}
	}

	public static Transaction toTransaction(ResultSet res) {
		try {
			res.next();
			int id = res.getInt(1);
			LocalDate date = res.getObject(2, LocalDate.class);
			int ownerId = res.getInt(3);
			int type = res.getInt(4);
			double amount = res.getDouble(5);
			int accountId = res.getInt(6);
			return new Transaction(id, date, ownerId, type, amount, accountId);
		} catch (SQLException e) {
			LOGGER.error("Could not create Transaction object :", e);
			return null;
		}
	}

	public static Connection getConnection() {
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream("resources/db.properties"));
			Class.forName(driver);
			Connection con = DriverManager.getConnection(prop.getProperty("url"), prop.getProperty("user"), prop.getProperty("pwd"));
			return con;
		} catch (ClassNotFoundException | SQLException e) {
			LOGGER.error("Could not get connection :", e);
			return null;
		} catch (IOException e) {
			LOGGER.error("Configuration file not found :", e);
			return null;
		}
	}

}
