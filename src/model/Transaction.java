package model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import model.utils.DBUtils;

public class Transaction {

	private int id;
	private LocalDate date;
	private int owner;
	private int type;
	private double amount;
	private int account;
	private static final Logger LOGGER = (Logger) LogManager.getLogger(Transaction.class);
	
	public Transaction(int id, LocalDate date, int ownerId, int type, double amount, int accountId) {
		this.setId(id);
		this.setDate(date);
		this.setOwner(ownerId);
		this.setType(type);
		this.setAmount(amount);
		this.setAccount(accountId);
	}
	
	public Transaction(LocalDate date, int ownerId, int type, double amount, int accountId) {
		this.setDate(date);
		this.setOwner(ownerId);
		this.setType(type);
		this.setAmount(amount);
		this.setAccount(accountId);
	}
	
	public static boolean addTransaction(Transaction t) {
		try {
			Connection con = DBUtils.getConnection();
			PreparedStatement st = con.prepareStatement("INSERT INTO transactions(date, owner, type, amount, account) VALUES(?,?,?,?,?)");
			st.setObject(1, t.getDate());
			st.setInt(2, t.owner);
			st.setInt(3, t.type);
			st.setDouble(4, t.getAmount());
			st.setInt(5, t.account);
			st.executeUpdate();
			return true;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;		
		}
	}
	
	public static Transaction getTransactionById(int id) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM transactions WHERE id=" + id);
			Transaction t = DBUtils.toTransaction(res);
			return t;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return null;
		}
	}
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public User getOwner() {
		return User.getUserById(owner) ;
	}

	public void setOwner(int ownerId) {
		this.owner = ownerId;
	}
	
	public Account getAccount() {
		return Account.getAccountById(account);
	}

	public void setAccount(int accountId) {
		this.account = accountId;
	}

	public TransactionType getType() {
		return TransactionType.values()[type-1];
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
