package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.utils.DBUtils;

public class User {

	private int id;
	private String lastName;
	private String firstName;
	private String username;
	private String password;
	public List<Integer> accounts;
	private String language;
	private String country;
	private static final Logger LOGGER = LogManager.getLogger(User.class);

	public User(int id, String firstName, String lastName, String username, String password, List<Integer> accounts, String language, String country) {
		this.setId(id);
		this.setLastName(lastName);
		this.setFirstName(firstName);
		this.setUsername(username);
		this.setPassword(password);
		this.setAccounts(accounts);
		this.setLanguage(language);
		this.setCountry(country);
	}
	
	public User(String firstName, String lastName, String username, String password, String language, String country) {
		this.setLastName(lastName);
		this.setFirstName(firstName);
		this.setUsername(username);
		this.setPassword(password);
		this.setLanguage(language);
		this.setCountry(country);
	}

	public static boolean addUser(User u) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			st.executeUpdate("INSERT INTO users(firstname, lastname, username, pwd, language, country) VALUES('" + u.getFirstName() + "','"
					+ u.getLastName() + "','" + u.getUsername() + "','" + u.getPassword() + "','"+u.getLanguage()+"','"+u.getCountry()+"') ON CONFLICT (username) DO NOTHING");
			return true;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;		
		}
		
	}

	public static List<User> getUsers() {
		List<User> list = new ArrayList<User>();
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM users");
			while (res.next()) {
				User u = DBUtils.toUser(res);
				list.add(u);
			}
			return list;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return null;
		}

	}

	public static User getUserById(int id) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM users WHERE id=" + id);
			if(res.isBeforeFirst()) {
				User u = DBUtils.toUser(res);
				return u;
			}
			return null;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return null;
		}
	}
	
	public boolean refresh() {
		try (Connection con = DBUtils.getConnection();Statement st = con.createStatement()){
			// USERS table
			ResultSet res = st.executeQuery("SELECT * FROM users WHERE id="+this.getId());
			User u = DBUtils.toUser(res);
			this.setId(u.id);
			this.setLastName(u.lastName);
			this.setFirstName(u.firstName);
			this.setUsername(u.username);
			this.setPassword(u.password);
			this.setAccounts(u.accounts);
			this.setLanguage(u.language);
			this.setCountry(u.country);
			return true;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;
		}
	}
	
	public boolean update() {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			// USERS table
			st.executeUpdate("UPDATE users SET firstname='"+this.getFirstName()+"',lastname='"+this.getLastName()+"',username='"+this.getUsername()+"',pwd='"+this.getPassword()+"', language='"+this.getLanguage()+"', country='"+this.getCountry()+"' WHERE id="+this.getId());
			// OWNS table
			st.executeUpdate("DELETE FROM owns WHERE id_user="+this.getId());
			for(int i: accounts) {
				st.executeUpdate("INSERT INTO owns(id_user, id_account) VALUES ("+this.getId()+","+i+")");
			}
			LOGGER.info("User updated !");
			return true;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;
		}
	}

	public static User auth(String username, String password) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM users WHERE username='"+username+"' AND pwd='"+password+"'");
			if(res.isBeforeFirst()) {
				LOGGER.info("User exists !");
				return DBUtils.toUser(res);
			}
			LOGGER.info("User does not exist.");
			return null;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return null;
		}
	}
	
	public static boolean isUsernameTaken(String username) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM users WHERE username='"+username+"'");
			if(res.isBeforeFirst()) {
				LOGGER.info("Username already exists !");
				return true;
			}
			LOGGER.info("Username does not exist.");
			return false;
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			// Return true to prevent username override
			return true;
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Account> getAccounts() {
		List<Account> list = new ArrayList<>();
		for(int i:this.accounts) {
			list.add(Account.getAccountById(i));
		}
		return list;
	}

	public void setAccounts(List<Integer> accounts) {
		if (accounts != null) {
			this.accounts = accounts;
		} else {
			this.accounts = new ArrayList<Integer>();
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
