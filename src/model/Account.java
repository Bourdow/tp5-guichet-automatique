package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import model.utils.DBUtils;

public class Account {

	private int id;
	private List<Integer> owners;
	private int type;
	private double amount;
	private double MAX_OVERDRAFT;
	private List<Integer> transactions;

	private static final Logger LOGGER = (Logger) LogManager.getLogger(Account.class);

	public Account(int id, List<Integer> owners, int type, double amount, double MAX_OVERDRAFT, List<Integer> transactions) {
		this.setId(id);
		this.setOwners(owners);
		this.setType(type);
		this.setAmount(amount);
		this.setOverdraft(MAX_OVERDRAFT);
		this.setTransactions(transactions);
	}

	public Account(List<Integer> owners, int type, double amount, double MAX_OVERDRAFT) {
		this.setOwners(owners);
		this.setType(type);
		this.setAmount(amount);
		this.setOverdraft(MAX_OVERDRAFT);
	}

	public static boolean addAccount(Account a) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			// Check if user has <= 2 accounts
			boolean canCreateAccount = true;
			for(int i: a.owners) {
				ResultSet rsOwner = st.executeQuery("SELECT COUNT(*) FROM owns WHERE id_user="+i);
				if(rsOwner.isBeforeFirst()) {
					rsOwner.next();
					if(rsOwner.getInt(1) >2) {
						canCreateAccount = false;
					}
				}
			}
			if(canCreateAccount) {
				st.execute("INSERT INTO accounts(type, amount, max_overdraft) VALUES(" + (a.getType().ordinal() + 1) + ", 1"
						+ "," + a.getOverdraft() + ") RETURNING id");
				ResultSet rs = st.getResultSet();
				rs.next();
				a.setId(rs.getInt(1));
				st.executeUpdate("DELETE FROM owns WHERE id_account="+a.getId());
				for(int i: a.owners) {
					st.executeUpdate("INSERT INTO owns(id_user, id_account) VALUES ("+i+","+a.getId()+")");
				}
				return true;
			}else {
				LOGGER.info("User already has 2 accounts (max limit reached)");
				return false;
			}
		} catch (SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;		
		}
	}

	public boolean withdraw(int owner, double amountToWithdraw) {
		amountToWithdraw = Math.abs(amountToWithdraw);
		if(this.getAmount() - amountToWithdraw >= -this.getOverdraft()) {
			this.setAmount(this.getAmount() - amountToWithdraw);
			Transaction.addTransaction(new Transaction(LocalDate.now(), owner, (TransactionType.RETRAIT.ordinal()+1), amountToWithdraw, this.getId()));
			update();
			return true;
		}
		LOGGER.error("Not enough funds to withdraw this amount ("+amountToWithdraw+")");
		return false;
	}

	public boolean deposit(int owner, double amountToDeposit) {
		amountToDeposit = Math.abs(amountToDeposit);
		this.setAmount(this.getAmount() + amountToDeposit);
		Transaction.addTransaction(new Transaction(LocalDate.now(), owner, (TransactionType.AJOUT.ordinal()+1), amountToDeposit, this.getId()));
		update();
		LOGGER.info(amountToDeposit+"eurodollarz added to account");
		return true;
	}

	/** TODO Il se passe des trucs pas nets dans cette m�thode, voir si c'est fonctionnel quand m�me **/
	public List<Transaction> getLast5Transactions(){
		List<Transaction> list = new ArrayList<>();
		ListIterator<Transaction> listIterator = this.getTransactions().listIterator(this.transactions.size());
		while(listIterator.hasPrevious() && list.size() < 5) {
			list.add(listIterator.previous());
		}
		return list;
	}

	public static Account getAccountById(int id) {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM accounts WHERE id="+id);
			return DBUtils.toAccount(res);
		}catch(SQLException e) {
			LOGGER.error("Query failed :", e);
		}
		return null;
	}

	public boolean update() {
		try {
			Connection con = DBUtils.getConnection();
			Statement st = con.createStatement();
			// ACCOUNTS table
			st.executeUpdate("UPDATE accounts SET type="+this.type+", amount="+this.getAmount()+", max_overdraft="+this.getOverdraft()+" WHERE id="+this.getId());
			// OWNS table
			st.executeUpdate("DELETE FROM owns WHERE id_account="+this.getId());
			for(int i: owners) {
				st.executeUpdate("INSERT INTO owns(id_user, id_account) VALUES ("+i+","+this.getId()+")");
			}
			LOGGER.info("Account updated !");
			return true;
		}catch(SQLException e) {
			LOGGER.error("Query failed :", e);
			return false;
		}
	}

	/*********************/
	/** Getters/Setters **/
	/*********************/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<User> getOwners() {
		List<User> list = new ArrayList<>();
		for(int i:this.owners) {
			list.add(User.getUserById(i));
		}
		return list;
	}

	public void setOwners(List<Integer> owners) {
		this.owners = owners;
	}


	public AccountType getType() {
		return AccountType.values()[type-1];
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getOverdraft() {
		return MAX_OVERDRAFT;
	}

	public void setOverdraft(double overdraft) {
		this.MAX_OVERDRAFT = overdraft;
	}

	public List<Transaction> getTransactions() {
		List<Transaction> list = new ArrayList<>();
		for(int i:this.transactions) {
			list.add(Transaction.getTransactionById(i));
		}
		return list;
	}

	public void setTransactions(List<Integer> transactions) {
		this.transactions = transactions;
	}


}

